class Mapa{

miVisor;
mapaBase;
posicionInicial;
escalaInicial;
proveedorURL;
atributosProveedor;
marcadores=[];
circulos=[];
poligonos=[];

constructor(){

    this.posicionInicial=[4.718833,-74.116003];
    this.escalaInicial=15;
    this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    this.atributosProveedor={
        maxZoom:20
    };

    this.miVisor=L.map("mapid");
    this.miVisor.setView(this.posicionInicial, this.escalaInicial);
    this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
    this.mapaBase.addTo(this.miVisor);


}

colocarMarcador(posicion){

    this.marcadores.push(L.marker(posicion));
    this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
}

colocarCirculo(posicion, configuracion){

    this.circulos.push(L.circle(posicion, configuracion)); 
    this.circulos[this.circulos.length-1].addTo(this.miVisor);    


}

colocarPoligono(posicion, configuracion){

    this.poligonos.push(L.polygon(posicion, configuracion));
    this.poligonos[this.poligonos.length-1].addTo(this.miVisor);

}

}

let miMapa=new Mapa();

miMapa.colocarMarcador([4.718833,-74.116003]);
miMapa.colocarCirculo([4.718833,-74.116003],{
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 500
    });

miMapa.colocarMarcador([4.715758, -74.113295])    
miMapa.colocarPoligono([4.725022, -74.112578],[4.725766, -74.116777],[4.727366, -74.119871]);
//miMapa.colocarMarcador(4.722353, -74.116731);